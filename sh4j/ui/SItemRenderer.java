package sh4j.ui;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SObject;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.model.command.SSortClassesByHierarchy;

@SuppressWarnings("serial")
public class SItemRenderer extends JLabel implements ListCellRenderer<SObject> {
  protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

  public SItemRenderer() {
    setOpaque(true);
    setBorder(BorderFactory.createLineBorder(Color.WHITE));
  }

  @Override
  public Component getListCellRendererComponent(JList<? extends SObject> list, SObject value,
      int index, boolean isSelected, boolean cellHasFocus) {
    if (value instanceof SClass) {
      SClass theclass = (SClass) value;
      @SuppressWarnings("unchecked")
      ListModel<SClass> model = (ListModel<SClass>) list.getModel();
      SPackage dummypackage = new SPackage(null);
      for (int i = 0; i < model.getSize(); i++) {
        dummypackage.addClass(model.getElementAt(i));
      }
      SProject dummyproject = new SProject();
      dummyproject.addPackage(dummypackage);
      new SSortClassesByHierarchy().executeOn(dummyproject);
      List<SClass> sortedclasses = dummyproject.packages().get(0).classes();
      setText(getIndentation(sortedclasses, theclass) + theclass.toString());
      
    }
    else {
      setText(value.toString());
    }
    setFont(value.font());
    setIcon(new ImageIcon(value.icon()));
    setBackground(value.background());


    if (isSelected) {
      setBorder(BorderFactory.createLineBorder(Color.BLUE));
    } else {
      setBorder(BorderFactory.createLineBorder(Color.WHITE));
    }
    return this;
  }
  
  /**
   * Returns the corresponding indentation of a class using the indentation of its super class.
   * This is a recursive method. 
   * @param classes The list of classes.
   * @param cls The class to be indented.
   * @return The indentation (spaces).
   */
  private String getIndentation(List<SClass> classes, SClass cls) {
    for (SClass auxclass : classes) {
      if (cls.superClass().equals(auxclass.className())) {
        return "    " + getIndentation(classes, auxclass);
      }
    }
    return "";
  }
  
}

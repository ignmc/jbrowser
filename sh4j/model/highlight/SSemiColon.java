package sh4j.model.highlight;

import sh4j.model.style.SStyle;

/**
 * This hightlighter can highlight semicolons.
 */
public class SSemiColon implements SHighlighter {

  @Override
  public boolean needsHighLight(String text) {
    return ";".equals(text);
  }

  @Override
  public String highlight(String text, SStyle style) {
    return style.formatSemiColon(text);
  }

}

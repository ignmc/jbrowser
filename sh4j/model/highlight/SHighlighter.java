package sh4j.model.highlight;

import sh4j.model.style.SStyle;

/**
 * This interface defines objects that can highlight text and check if they need to be highlighted.
 */
public interface SHighlighter {
  
  /**
   * Checks whether the given argument needs highlight or not.
   * @param text The text to be highlighted
   */
  public boolean needsHighLight(String text);
  
  /**
   * Highlights the a string using a given style.
   * @param text The text to be Highlighted
   * @param style The style to be used
   */
  public String highlight(String text, SStyle style);
}

package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.Comparator;

/**
 * This class represents a command for sorting the classes of a java project by name.
 */
public class SSortClassesByName extends SCommand {


  @Override
  public void executeOn(SProject project) {

    for (SPackage pkg : project.packages()) {
      Collections.sort(pkg.classes(), new Comparator<SClass>() {
        @Override
        public int compare(SClass o1, SClass o2) {
          return o1.toString().compareTo(o2.toString());
        }
      });
    }
    project.changed();
  }
}

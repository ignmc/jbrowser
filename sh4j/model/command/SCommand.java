package sh4j.model.command;


import sh4j.model.browser.SProject;

/**
 * Represents an applicable command to a project.
 */
public abstract class SCommand {

  public abstract void executeOn(SProject project);

  public String name() {
    return this.getClass().getName();
  }
}

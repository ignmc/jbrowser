package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class represents a command for sorting the classes of a java project by Hierarchy.
 */
public class SSortClassesByHierarchy extends SCommand {

  @Override
  public void executeOn(SProject project) {
    (new SSortClassesByName()).executeOn(project);
    for (final SPackage pkg : project.packages()) {
      Collections.sort(pkg.classes(), new Comparator<SClass>() {
        @Override
        public int compare(SClass o1, SClass o2) {
          String path1 = path(pkg.classes(), o1);
          String path2 = path(pkg.classes(), o2);
          return path1.compareTo(path2);
        }
      });
      
    }
    project.changed();
  }

  /**
   * Get the path of a class of a project.
   * @param cls The list with the classes.
   * @param theClass The class to search for.
   * @return The path of the class.
   */
  public String path(List<SClass> cls, SClass theClass) {
    SClass parent = get(cls, theClass.superClass());
    if (parent == null) {
      return theClass.toString();
    } else {
      return path(cls, parent) + theClass.toString();
    }
  }

  /**
   * Returns the class of a list of classes.
   */
  public SClass get(List<SClass> cls, String theClass) {
    for (SClass c : cls) {
      if (c.toString().equals(theClass)) {
        return c;
      }
    }
    return null;
  }

}

package sh4j.model.style;

/**
 * This class represents the bred style for text formatting.
 */
public class SBredStyle implements SStyle {

  @Override
  public String toString() {
    return "bred";
  }

  @Override
  public String formatClassName(String text) {
    return text;
  }

  @Override
  public String formatCurlyBracket(String text) {
    return "<span style='color:#806030; '>" + text + "</span>";
  }

  @Override
  public String formatKeyWord(String text) {
    return "<span style='color:#800040; '>" + text + "</span>";
  }

  @Override
  public String formatPseudoVariable(String text) {
    return "<span style='color:#400000; font-weight:bold; '>" + text + "</span>";
  }

  @Override
  public String formatSemiColon(String text) {
    return "<span style='color:#806030; '>" + text + "</span>";
  }

  @Override
  public String formatString(String text) {
    return "<span style='color:#e60000; '>" + text + "</span>";
  }

  @Override
  public String formatMainClass(String text) {
    return "<span style='color:#800040; '>" + text + "</span>";
  }

  @Override
  public String formatBody(String text) {
    return "<pre style='color:#000000;background:#f1f0f0;'>" + text + "</pre>";
  }

  @Override
  public String formatModifier(String text) {
    return "<span style='color:#400000; font-weight:bold; '>" + text + "</span>";
  }

}

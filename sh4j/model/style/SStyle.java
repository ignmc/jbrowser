package sh4j.model.style;

/**
 * This interface represents an object able to check if a String needs highlight and highlight it. 
 */
public interface SStyle {

  /**
   * Returns the name of the style.
   */
  public String toString();

  /**
   * Returns the text with the class name style applied.
   * @param text The text to stylized
   */
  public String formatClassName(String text);
  
  /**
   * Returns the text with the bracket style applied.
   * @param text The text to be stylized
   */
  public String formatCurlyBracket(String text);
  
  /**
   * Returns the text with the keyword style applied.
   * @param text The text to be stylized
   */
  public String formatKeyWord(String text);

  /**
   * Returns the text with the pseudovariable style applied.
   * @param text The text to be stylized
   */
  public String formatPseudoVariable(String text);
  
  /**
   * Returns the text with the semicolon style applied.
   * @param text The text to be stylized
   */
  public String formatSemiColon(String text);
  
  /**
   * Returns the text with the string style applied.
   * @param text The text to be stylized
   */
  public String formatString(String text);
  
  /**
   * Returns the text with the class name style applied.
   * @param text The text to be stylized
   */
  public String formatMainClass(String text);
  
  /**
   * Returns the text with the modifier style applied.
   * @param text The text to be stylized
   */
  public String formatModifier(String text);
  
  /**
   * Stylises a string with a given parameter and using Html code.
   * @param style The parameter used for stylize the text
   * @param text The text to be stylized
   */
  public String formatBody(String text);


}

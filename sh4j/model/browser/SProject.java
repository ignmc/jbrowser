package sh4j.model.browser;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a java project.
 */
public class SProject extends SObject {
  private final List<SPackage> packages;

  public SProject() {
    packages = new ArrayList<SPackage>();
  }

  public void addPackage(SPackage pack) {
    packages.add(pack);
  }

  public List<SPackage> packages() {
    return packages;
  }

  /**
   * Search a package of this project.
   * @param pkgName The package to search.
   */
  public SPackage get(String pkgName) {
    for (SPackage pkg : packages) {
      if (pkg.toString().equals(pkgName)) {
        return pkg;
      }
    }
    return null;
  }

  /**
   * Returns the total number of lines of this project.
   */
  public int getLinesOfCode() {
    int counter = 0;
    for (SPackage p : packages) {
      counter += p.getLinesOfCode();
    }
    return counter;
  }

  /**
   * Returns the number of classes of this project. 
   */
  public int getNumberOfClasses() {
    int counter = 0;
    for (SPackage p : packages) {
      counter += p.getNumberOfClasses();
    }
    return counter;
  }

  /**
   * Returns the number of method with more than 30 lines of code of this project.
   */
  public int getNumberOfWarnings() {
    int counter = 0;
    for (SPackage p : packages) {
      counter += p.getNumberOfWarnings();
    }
    return counter;
  }

}

package sh4j.model.browser;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import java.awt.Font;
import java.util.List;

/**
 * This class represents a class of a package.
 */
public class SClass extends SObject {
  private final TypeDeclaration declaration;
  private final List<SMethod> methods;

  public SClass(TypeDeclaration td, List<SMethod> ms) {
    declaration = td;
    methods = ms;
  }

  public List<SMethod> methods() {
    return methods;
  }

  public String className() {
    return declaration.getName().getIdentifier();
  }

  public boolean isInterface() {
    return declaration.isInterface();
  }

  /**
   * Get the superclass of this class.
   */
  public String superClass() {
    if (declaration.getSuperclassType() == null) {
      return "Object";
    }
    return declaration.getSuperclassType().toString();
  }

  @Override
  public String toString() {
    return className();
  }

  @Override
  public Font font() {
    if (isInterface()) {
      return new Font("Helvetica", Font.ITALIC, 12);
    }
    return super.font();
  }

  /**
   * Returns the icon used to show this class.
   */
  public String icon() {
    if (isInterface()) {
      return "./resources/int_obj.gif";
    }
    return "./resources/class_obj.gif";
  }

  /**
   * Returns the number of lines that constitutes this class.
   */
  public int getLinesOfCode() {
    int counter = 0;
    for (SMethod m : methods) {
      counter += m.getLinesOfCode();
    }
    return counter + 1;
  }

  /**
   * Returns the number of methods of this class with more than 30 lines of code.
   */
  public int getNumberOfWarnings() {
    int counter = 0;
    for (SMethod m : methods) {
      if (m.getLinesOfCode() > 30) {
        counter++;
      }
    }
    return counter;
  }
}

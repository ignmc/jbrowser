package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;

import sh4j.parser.model.SBlock;

import java.awt.Color;

/**
 * This class represents the method of a class.
 */
public class SMethod extends SObject {
  private final MethodDeclaration declaration;
  private final SBlock body;

  public SMethod(MethodDeclaration node, SBlock body) {
    declaration = node;
    this.body = body;
  }

  /**
   * Returns the modifier of this method.
   */
  public String modifier() {
    for (Object obj : declaration.modifiers()) {
      if (obj instanceof Modifier) {
        Modifier modifier = (Modifier) obj;
        return modifier.getKeyword().toString();
      }
    }
    return "default";
  }

  public String name() {
    return declaration.getName().getIdentifier();
  }

  public SBlock body() {
    return body;
  }

  @Override
  public String toString() {
    return name();
  }

  public int getLinesOfCode() {
    String definition = this.body().toString();
    return definition.length() - definition.replace("\n", "").length();
  }

  public String icon() {
    return "./resources/" + modifier() + "_co.gif";
  }

  @Override
  public Color background() {
    if (getLinesOfCode() > 50) {
      return Color.RED;
    }
    if (getLinesOfCode() > 30) {
      return Color.YELLOW;
    }
    return super.background();
  }
}

package sh4j.model.browser;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a package of a java project.
 */
public class SPackage extends SObject {

  private final String name;
  private final List<SClass> classes;

  public SPackage(String name) {
    classes = new ArrayList<SClass>();
    this.name = name;
  }

  public void addClass(SClass cls) {
    classes.add(cls);
  }

  public List<SClass> classes() {
    return classes;
  }

  public String toString() {
    return name;
  }

  public boolean isEmpty() {
    return classes.isEmpty();
  }

  /**
   * Returns the icon used to show this package.
   */
  public String icon() {
    if (isEmpty()) {
      return "./resources/pack_empty_co.gif";
    }
    return "./resources/package_mode.gif";
  }

  /**
   * Returns the lines of code of this package.
   */
  public int getLinesOfCode() {
    int counter = 0;
    for (SClass c : classes) {
      counter += c.getLinesOfCode();
    }
    return counter;
  }

  /**
   * Returns the number of classes of this package.
   */
  public int getNumberOfClasses() {
    return classes.size();
  }

  /**
   * Returns the number of methods with more than 30 lines of code of this package.
   */
  public int getNumberOfWarnings() {
    int counter = 0;
    for (SClass c : classes) {
      counter += c.getNumberOfWarnings();
    }
    return counter;
  }
  
  /**
   * Search for a class in this package.
   * @param  clsname The class to search.
   */
  public SClass get(String clsname) {
    for (SClass cls : classes) {
      if (cls.toString().equals(clsname)) {
        return cls;
      }
    }
    return null;
  }
}

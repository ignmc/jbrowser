package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
import java.util.Observable;

/**
 * This class represents any element of a java project.
 */
public class SObject extends Observable {

  /**
   * Return the font used to show this object.
   */
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  /**
   * Returns the icon which represents this object.
   */
  public String icon() {
    return "";
  }

  /**
   * Returns the background color used to show this object.
   */
  public Color background() {
    return Color.WHITE;
  }

  public void changed() {
    setChanged();
    notifyObservers();
  }

}

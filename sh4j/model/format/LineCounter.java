package sh4j.model.format;

/**
 * An object of this class can format the lines of a text enumerating every line of it.
 */
public class LineCounter implements ILineCounter {

  @Override
  public String getLineTag(int line) {
    String number = line < 10 ? " " + line : "" + line;
    return "<span style='background:#f1f0f0;'> " + number + " </span>";
  }

  @Override
  public String toString() {
    return "Show Line Numbers";
  }

}

package sh4j.model.format;

import sh4j.model.highlight.SDummy;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;
import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;

/**
 * An object of this class highlights text using Html code.
 */
public class SHTMLFormatter implements SFormatter {
  private final StringBuffer buffer;
  private int level;
  private final SStyle style;
  private final SHighlighter[] highlighters;
  private int lineCount;
  private final ILineCounter lineformatter;

  /**
   * Creates a new highlighter.
   */
  public SHTMLFormatter(SStyle style, ILineCounter lineformatter, SHighlighter... hs) {
    this.style = style;
    this.lineformatter = lineformatter;
    highlighters = hs;
    buffer = new StringBuffer();
    lineCount = 0;
  }

  private SHighlighter lookup(String text) {
    for (SHighlighter h : highlighters) {
      if (h.needsHighLight(text)) {
        return h;
      }
    }
    return new SDummy();
  }

  @Override
  public void styledWord(String word) {
    buffer.append(lookup(word).highlight(word, style));
  }

  @Override
  public void styledChar(char character) {
    buffer.append(lookup(character + "").highlight(character + "", style));
  }

  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  @Override
  public void styledCR() {

    lineCount++;
    buffer.append("\n");
    buffer.append(lineformatter.getLineTag(lineCount));
    indent();
  }

  @Override
  public void styledBlock(SBlock block) {
    level++;
    for (SText text : block.texts()) {
      text.export(this);
    }
    level--;
  }

  /**
   * Adds spaces according to the current indentation level.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  @Override
  public String formattedText() {
    return style.formatBody(buffer.toString());
  }

  public static String tag(String name, String content, String style) {
    return "<" + name + " style='" + style + "'>" + content + "</" + name + ">";
  }
}

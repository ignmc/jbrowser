package sh4j.model.format;

import sh4j.parser.model.SBlock;

/**
 * This interface defines objects that are supposed to format a text.
 */
public interface SFormatter {
  
  public void styledWord(String word);

  public void styledChar(char character);

  public void styledSpace();

  public void styledCR();

  public void styledBlock(SBlock block);

  public String formattedText();
}

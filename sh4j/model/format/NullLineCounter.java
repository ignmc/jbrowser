package sh4j.model.format;

/**
 * This formatter doesn't modify the line tag at all.
 */
public class NullLineCounter implements ILineCounter {

  @Override
  public String getLineTag(int line) {
    return "";
  }

  @Override
  public String toString() {
    return "Don't show LineNumbers";
  }

}

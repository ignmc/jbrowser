package sh4j.model.format;

/**
 * Represents an object that can format the lines of a given text.
 */
public interface ILineCounter {
  /**
   * Formats the line using the line number.
   * @param line The number of the line
   */
  public String getLineTag(int line);

}

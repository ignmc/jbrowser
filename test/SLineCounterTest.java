package test;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.List;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.format.LineCounter;
import sh4j.model.format.NullLineCounter;
import sh4j.model.style.SEclipseStyle;
import sh4j.parser.SClassParser;

public class SLineCounterTest {

  @Test
  public void LineCounterTest() {
    List<SClass> cls = SClassParser
        .parse("class A{ public int foo(){\n int a;\n int b;\n int c=a+b; return c; }}");
    SMethod method = cls.get(0).methods().get(0);
    String html = method.body().toHTML(new SEclipseStyle(), new LineCounter());
    assertEquals(
        "<pre style='color:#000000;background:#ffffff;'>\n<span style='background:#f1f0f0;'>  1 </span>  public int foo(){\n<span style='background:#f1f0f0;'>  2 </span>    int a;\n<span style='background:#f1f0f0;'>  3 </span>    int b;\n<span style='background:#f1f0f0;'>  4 </span>    int c=a + b;\n<span style='background:#f1f0f0;'>  5 </span>    return c;\n<span style='background:#f1f0f0;'>  6 </span>    \n<span style='background:#f1f0f0;'>  7 </span>  }</pre>",
        html);
  }
  
  @Test
  public void NullLineCounterTest() {
    List<SClass> cls = SClassParser
        .parse("class A{ public int foo(){\n int a;\n int b;\n int c=a+b; return c; }}");
    SMethod method = cls.get(0).methods().get(0);
    String html = method.body().toHTML(new SEclipseStyle(), new NullLineCounter());
    assertEquals(
        "<pre style='color:#000000;background:#ffffff;'>\n  public int foo(){\n    int a;\n    int b;\n    int c=a + b;\n    return c;\n    \n  }</pre>",
        html);
  }
}

package test;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.List;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.parser.SClassParser;

public class SDimensionTest {

  @Test
  public void LinesOfCodeTest() {
    List<SClass> cls = SClassParser
        .parse("class A{ public int foo(){\n int a;\n int b;\n int c=a+b; return c; }}");
    SPackage pck = new SPackage(null);
    pck.addClass(cls.get(0));
    SProject project = new SProject();
    project.addPackage(pck);
    assertEquals(8, project.getLinesOfCode());
  }

  @Test
  public void WarningsTest() {
    SPackage pck = new SPackage(null);
    List<SClass> cls = SClassParser
        .parse("class A{ public int foo(){\n int a = 0;\n a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++; return a; }}");
    pck.addClass(cls.get(0));
    cls = SClassParser
        .parse("class A{ public int foo(){\n int a = 0;\n a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++; return a; }}");
    pck.addClass(cls.get(0));
    SProject project = new SProject();
    project.addPackage(pck);
    assertEquals(2, project.getNumberOfWarnings());
  }
  
  @Test
  public void NumberOfClassesTest() {
    SPackage pck = new SPackage(null);
    List<SClass> cls = SClassParser
        .parse("class A{ public int foo(){\n int a = 0;\n a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++;a++; return a; }}");
    pck.addClass(cls.get(0));
    cls = SClassParser
        .parse("class A{ public int foo(){\n int a;\n int b;\n int c=a+b; return c; }}");
    pck.addClass(cls.get(0));
    SProject project = new SProject();
    project.addPackage(pck);
    assertEquals(2, project.getNumberOfClasses());
  }
}
